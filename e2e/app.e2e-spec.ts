import { ParseServerExamplePage } from './app.po';

describe('parse-server-example App', function() {
  let page: ParseServerExamplePage;

  beforeEach(() => {
    page = new ParseServerExamplePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
