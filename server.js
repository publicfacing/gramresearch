/**
 * Created by keeanomartin on 5/24/17.
 */
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var tasks = require('./routes/tasks');

var port = 3000;

var app = express();

//View Engine
app.set('views', path.join(__dirname, './client/views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

//set static folder, gram-research is our static folder
app.use(express.static(path.join(__dirname, 'client')));

//MW
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use('/', index);
app.use('/api', tasks);

app.listen(port, function() {
    console.log('server started on port ' + port);
});