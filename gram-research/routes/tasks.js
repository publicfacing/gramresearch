/**
 * Created by keeanomartin on 5/24/17.
 */
var express = require('express');
var mongojs = require('mongojs');
var router = express.Router();

var db = mongojs('mongodb://gram:tHsrW5mz@ds057386.mlab.com:57386/gram-research-dev', ['SessionItem']);

router.get('/tasks', function(req, res, next){
    db.SessionItem.find(function(error, tasks){
        if(error){
            res.send(error);
        }
        res.json(tasks);
    })
});

//Not working but should return one item from DB at a time
router.get('/task/:id', function(req, res, next){
    db.SessionItem.findOne({_id: mongojs.ObjectID(req.params.id)}, function(error, task){
        if(error){
            res.send(error);
        }
        res.json(task);
    })
});

module.exports = router;