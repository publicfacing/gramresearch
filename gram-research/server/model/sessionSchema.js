/**
 * Created by keeanomartin on 5/22/17.
 */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var sessionSchema = new Schema({
  objectId: String,
  createdAt: Date,
  updatedAt: Date,
  ACL: String,
  date: Date,
  vape_pulses: Array,
  peripheral_id: String,
  total_mg: Number
});

var Session = mongoose.model('Session', sessionSchema);

module.exports = Session;
