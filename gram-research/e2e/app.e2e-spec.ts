import { GramResearchPage } from './app.po';

describe('gram-research App', function() {
  let page: GramResearchPage;

  beforeEach(() => {
    page = new GramResearchPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
