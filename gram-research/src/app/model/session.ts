/**
 * Created by keeanomartin on 5/24/17.
 */

export class Session {
  constructor(
  public objectId: string,
  public createdAt: string,
  public updatedAt: string,
  public ACL: string,
  public date: string,
  public vape_pulses: any[] = [],
  public peripheral_id: string,
  public total_mg: number){}
}
