import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { Session } from './model/session';

//import { ContextView } from './app.component';

@Injectable()

export class HttpClientService {

  private API_URL = 'https://gram-research-dev.herokuapp.com/peripherals';

  constructor(private http: Http) {}

  //create headers object to handle basicAuth
  createAuthorizationHeader(headers: Headers) {
    headers.append('Authorization', 'Basic' +
    btoa('admin:tHsrW5mz'));
    headers.append('Content-Type', 'application/json');
  }

  get(url) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.get(url, {
      headers: headers
    });
  }


  getSessions(): Observable<Session[]> {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.get(this.API_URL, {
      headers: headers
    }).map(this.extractData);
  }

  private extractData(res: Response) {
    return res.json();
  }

  //getSessions(): Observable<any[]> {
  //  return this.http.get(this.API_URL).map(this.extractData).catch(this.handleError);
  //}
  //
  //private extractData(res: Response) {
  //  let body = res.json();
  //  return body.data || {};
  //}
  //
  //private handleError (error: Response | any) {
  //  let errMsg: string;
  //  if(error instanceof Response){
  //    const body = error.json() || '';
  //    const err = body.error || JSON.stringify(body);
  //
  //    errMsg = '${error.status} - ${error.statusText || ''} ${err}';
  //
  //  } else {
  //    errMsg = error.message ? error.message : error.toString();
  //  }
  //  console.error(errMsg);
  //  return Observable.throw(errMsg);
  //}
}
