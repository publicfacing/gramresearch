import { Component } from '@angular/core';

//services
import { HttpClientService } from './http-client.service';

//model
import { Session } from './model/session';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [HttpClientService]
})
export class AppComponent{
  title = "The damn page actually loaded";
}
