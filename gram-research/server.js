var express = require("express");
var bodyParser = require("body-parser");
var mongodb = require("mongodb");
var ObjectID = mongodb.ObjectID;

var USER_COLLECTION = "SessionItem";


var app = express();
app.use(bodyParser.json());

var db;

var allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

  if ('OPTIONS' == req.method) {
    res.send(200);
  }
  else {
    next();
  }
};

app.use(allowCrossDomain);

mongodb.MongoClient.connect("mongodb://gram:tHsrW5mz@ds057386.mlab.com:57386/gram-research-dev", function (err, database){
  if(err){
    console.log(err);
    process.exit(1);
  }

  db = database;
  console.log("Database connection established...");

  //init application
  var server = app.listen(process.env.PORT || 8080, function () {
    var port = server.address().port;
    console.log("App is now running on port (" + port +")");
  });
});

//API Routes *Start*
function handlerErro(res, reason, message, code) {
  console.log("ERROR: " + reason);
  res.status(code || 500).json({"error": message});
}

/* GET All Users /api/users */
app.get("/api/users", function(req, res){
  db.collection(USER_COLLECTION).find({}).toArray(function(err, docs){
    if (err) {
      handlerError(res, err.message, "Failed to get users.");
    } else {
      res.status(200).json(docs);
    }
  });
});

/* POST New User /api/users */
app.get("/api/users", function(req, res){
  var newUser = req.body;

  if (!req.body.name) {
    handlerError(res, "Invalid user input", "Must provide a name.", 400);
  }
  db.collection(USER_COLLECTION).insertOne(newUser, function(err, doc){
    if(err){
      handleError(res, err.mesage, "Failed to create new user.");
    } else {

      res.status(201).json(doc.ops[0]);
    }
  });
});

/* GET Single User /api/users/:id */
app.get("/api/users/:id", function(req, res){

});

/* PUT/Update single user */
app.put("/api/users/:id", function(req, res){

});

/* DELETE User by ID (Add security at some point) */
app.delete("/api/users/:id", function(req, res){

});
