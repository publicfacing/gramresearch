import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MODULE_COMPONENTS, MODULE_ROUTES } from './dashboard.routes';

//Custom Pipe
import { RoundPipe } from './../pipes/round.number';

@NgModule({
    imports: [
        RouterModule.forChild(MODULE_ROUTES),
        BrowserModule,
        FormsModule,
        HttpModule
    ],
    declarations: [MODULE_COMPONENTS, RoundPipe]
})

export class DashboardModule{}
