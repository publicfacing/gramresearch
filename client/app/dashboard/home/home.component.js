"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var _ = require('underscore');
var session_service_1 = require('./../../services/session.service');
var HomeComponent = (function () {
    //Inject service here to use for getting data
    function HomeComponent(sessionService) {
        this.sessionService = sessionService;
    }
    HomeComponent.prototype.ngOnInit = function () {
        //Set View Data with Request from Server
        this.vapeShow = true;
        this.getSessionsForList();
    };
    HomeComponent.prototype.getSessionsForList = function () {
        var _this = this;
        this.busy = this.sessionService.getSessions()
            .subscribe(function (sessions) { return _this.sessions = _this.formSessionData(sessions); }, function (error) { return _this.errorMessage = error; });
    };
    //manipulate the data model appropriatly
    HomeComponent.prototype.formSessionData = function (sessions) {
        var _this = this;
        //remove from array where Periph ID is not there
        this.sessions = _.filter(sessions, function (session) {
            if (session.peripheral_id != '') {
                return session;
            }
        });
        //replace vape pulses with a number from the total of pulses
        _.each(sessions, function (session) {
            _this.vapePulses = session.vape_pulses;
            var lengthOfPulses = _this.vapePulses.length;
            console.log("Length of Pulse: " + lengthOfPulses);
            if (lengthOfPulses) {
                console.log("Value not Null");
                session.vape_pulses = lengthOfPulses;
                console.log("New Vape Pulse Value: " + session.vape_pulses);
            }
        });
        return sessions;
    };
    HomeComponent.prototype.toggleVapeTotal = function () {
        this.vapeShow = false;
    };
    HomeComponent = __decorate([
        core_1.Component({
            selector: 'home-cmp',
            moduleId: module.id,
            templateUrl: 'home.component.html',
            providers: [session_service_1.SessionService]
        }), 
        __metadata('design:paramtypes', [session_service_1.SessionService])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=home.component.js.map