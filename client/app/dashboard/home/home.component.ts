import { Component, OnInit, trigger, state, style, transition, animate } from '@angular/core';
import * as _ from 'underscore';
import { SessionService } from './../../services/session.service';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs';

//pipes
import { RoundPipe } from './../../pipes/round.number';

declare var $:any;

@Component({
    selector: 'home-cmp',
    moduleId: module.id,
    templateUrl: 'home.component.html',
    providers:[SessionService]
})
export class HomeComponent implements OnInit{
    busy: Subscription;
    sessions: Array<any>;
    errorMessage: string;
    vapeShow: boolean;
    vapePulses: Array<string>;
    //Inject service here to use for getting data
    constructor(private sessionService: SessionService){}

    ngOnInit(){
        //Set View Data with Request from Server
        this.vapeShow = true;
        this.getSessionsForList();
    }

    getSessionsForList(){
       this.busy = this.sessionService.getSessions()
                    .subscribe(
                        sessions => this.sessions = this.formSessionData(sessions),
                        error => this.errorMessage = <any>error
                    );
    }

      //manipulate the data model appropriatly
    formSessionData(sessions: Array<any>): Array<any> {
        //remove from array where Periph ID is not there
       this.sessions = _.filter(sessions, function(session){
            if(session.peripheral_id != ''){
                return session;
            }
        });

        //replace vape pulses with a number from the total of pulses
        _.each(sessions, session => {
            this.vapePulses = session.vape_pulses;
            let lengthOfPulses = this.vapePulses.length;
            console.log("Length of Pulse: " + lengthOfPulses);
            if(lengthOfPulses) {
                console.log("Value not Null");
                session.vape_pulses = lengthOfPulses;
                console.log("New Vape Pulse Value: " + session.vape_pulses);
            }
        });

        return sessions;
    }

    toggleVapeTotal(): void {
        this.vapeShow = false;
    }
}
