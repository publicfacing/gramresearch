"use strict";
var Session = (function () {
    function Session(sessionVariables) {
        this.id = sessionVariables.id;
        this.vape_pulses = sessionVariables.vape_pulses;
        this.peripheral_id = sessionVariables.peripheral_id;
        this.total_mg = sessionVariables.total_mg;
        this.date = sessionVariables.date;
        this._created_at = sessionVariables._created_at;
        this._updated_at = sessionVariables._updated_at;
    }
    return Session;
}());
exports.Session = Session;
//# sourceMappingURL=session.model.js.map