export class Session {
  id: string;
  vape_pulses: Array<string>;
  peripheral_id: string;
  total_mg: number;
  date: string;
  _created_at: string;
  _updated_at: string;

  constructor(sessionVariables: any) {
    this.id = sessionVariables.id;
    this.vape_pulses = sessionVariables.vape_pulses;
    this.peripheral_id = sessionVariables.peripheral_id;
    this.total_mg = sessionVariables.total_mg;
    this.date = sessionVariables.date;
    this._created_at = sessionVariables._created_at;
    this._updated_at = sessionVariables._updated_at;
  }
}
